const path = require('path');

const UnitTestSCAD = require('../../../src');

const openSCADDirectory = path.join(__dirname, '_resources');

const boolean = {
  file: 'boolean.scad',
  method: 'isBoolean',
  type: UnitTestSCAD.Types.BOOLEAN,
};
const inf = {
  file: 'inf.scad',
  method: 'isInf',
  type: UnitTestSCAD.Types.INF,
};
const nan = {
  file: 'nan.scad',
  method: 'isNan',
  type: UnitTestSCAD.Types.NAN,
};
const number = {
  file: 'number.scad',
  method: 'isNumber',
  type: UnitTestSCAD.Types.NUMBER,
};
const range = {
  file: 'range.scad',
  method: 'isRange',
  type: UnitTestSCAD.Types.RANGE,
};
const string = {
  file: 'string.scad',
  method: 'isString',
  type: UnitTestSCAD.Types.STRING,
};
const undef = {
  file: 'undef.scad',
  method: 'isUndef',
  type: UnitTestSCAD.Types.UNDEF,
};
const vector = {
  file: 'vector.scad',
  method: 'isVector',
  type: UnitTestSCAD.Types.VECTOR,
};
const types = [boolean, inf, nan, number, range, string, undef, vector];

const getTestee = (file, variables) => {
  return new UnitTestSCAD.Function({
    openSCADDirectory,
    use: [file],
    testText: 'test();',
    variables,
  });
};

describe('Function', () => {
  describe.each(
    types.map((type) => [type.type, type.file, type.method])
  )('%s', (type, file, method) => {
    const testee = getTestee(file, {});

    it('should be the correct type', () => expect(testee.type).toBe(type));
    it(`${type.method} should be true`, () => expect(testee[method]()).toBe(true, method));

    types
      .filter(notType => notType.type !== type)
      .forEach(notType => {
        it(`${notType.method} should be false`, () => expect(testee[notType.method]()).toBe(false, notType.method));
      });
  });

  describe('should use variables', () => {
    it('with boolean', () => {
      expect(getTestee(boolean.file, { value: false }).output).toBe('false');
      expect(getTestee(boolean.file, { value: true }).output).toBe('true');
    });

    it('with number', () => {
      expect(getTestee(number.file, { value: 1 }).output).toBe('1');
      expect(getTestee(number.file, { value: 1245 }).output).toBe('1245');
      expect(getTestee(number.file, { value: 3.1415 }).output).toBe('3.1415');
    });

    it('with range', () => {
      expect(getTestee(range.file, { value: '[-1 : 2 : 1]' }).output)
        .toBe('[-1 : 2 : 1]');
      expect(getTestee(range.file, { value: '[0 : 5]' }).output)
        .toBe('[0 : 1 : 5]');
      expect(getTestee(range.file, { value: '[1 : 10 : 100]' }).output)
        .toBe('[1 : 10 : 100]');
    });

    it('with string', () => {
      expect(getTestee(string.file, { value: '"foo"' }).output).toBe('"foo"');
      expect(getTestee(string.file, { value: '"bar"' }).output).toBe('"bar"');
      expect(getTestee(string.file, { value: '"baz"' }).output).toBe('"baz"');
    });

    it('with vector', () => {
      expect(getTestee(vector.file, { value: '[0, 1]' }).output).toBe('[0, 1]');
      expect(getTestee(vector.file, { value: '[[-5, -5], [5, 5]]' }).output)
        .toBe('[[-5, -5], [5, 5]]');
    });
  });
});
