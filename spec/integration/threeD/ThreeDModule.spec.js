const path = require('path');

const UnitTestSCAD = require('../../../src');

const openSCADDirectory = path.join(__dirname, '_resources');

describe('ThreeDModule', () => {
  describe('error', () => {
    it('should error on a bad .scad file', () => {
      expect(() => new UnitTestSCAD.ThreeDModule({
        openSCADDirectory: path.join(__dirname, '..', '_resources'),
        use: ['garbage.scad'],
      })).toThrow();
    });
  });

  describe('success', () => {
    const expectedVertices = [
      [0, 4, 6],
      [5, 0, 6],
      [5, 4, 6],
      [0, 0, 6],
      [0, 0, 0],
      [5, 4, 0],
      [5, 0, 0],
      [0, 4, 0],
    ];
    const expectedTriangles = [
      [[0, 4, 6], [5, 0, 6], [5, 4, 6]],
      [[5, 0, 6], [0, 4, 6], [0, 0, 6]],
      [[0, 0, 0], [5, 4, 0], [5, 0, 0]],
      [[5, 4, 0], [0, 0, 0], [0, 4, 0]],
      [[0, 0, 0], [5, 0, 6], [0, 0, 6]],
      [[5, 0, 6], [0, 0, 0], [5, 0, 0]],
      [[5, 0, 6], [5, 4, 0], [5, 4, 6]],
      [[5, 4, 0], [5, 0, 6], [5, 0, 0]],
      [[5, 4, 0], [0, 4, 6], [5, 4, 6]],
      [[0, 4, 6], [5, 4, 0], [0, 4, 0]],
      [[0, 0, 0], [0, 4, 6], [0, 4, 0]],
      [[0, 4, 6], [0, 0, 0], [0, 0, 6]]
    ];

    describe('include', () => {
      let testee;

      beforeEach(() => {
        testee = new UnitTestSCAD.ThreeDModule({
          openSCADDirectory,
          include: ['cube.scad'],
        });
      });

      it('should have the correct width', () => expect(testee.width).toBe(5));
      it('should have the correct depth', () => expect(testee.depth).toBe(4));
      it('should have the correct height', () => expect(testee.height).toBe(6));
      it('should have the correct vertices', () => expect(testee.vertices).toEqual(expectedVertices));
      it('should have the correct triangles', () => expect(testee.triangles).toEqual(expectedTriangles));
    });

    describe('use', () => {
      let testee;

      beforeEach(() => {
        testee = new UnitTestSCAD.ThreeDModule({
          openSCADDirectory,
          use: ['cube-module.scad'],
          testText: 'myCube([5, 4, 6]);'
        });
      });

      it('should have the correct width', () => expect(testee.width).toBe(5));
      it('should have the correct depth', () => expect(testee.depth).toBe(4));
      it('should have the correct height', () => expect(testee.height).toBe(6));
      it('should have the correct vertices', () => expect(testee.vertices).toEqual(expectedVertices));
      it('should have the correct triangles', () => expect(testee.triangles).toEqual(expectedTriangles));
    });

    describe('setUpText', () => {
      let testee;

      beforeEach(() => {
        testee = new UnitTestSCAD.ThreeDModule({
          openSCADDirectory,
          setUpText: 'cube([5, 4, 6]);'
        });
      });

      it('should have the correct width', () => expect(testee.width).toBe(5));
      it('should have the correct depth', () => expect(testee.depth).toBe(4));
      it('should have the correct height', () => expect(testee.height).toBe(6));
      it('should have the correct vertices', () => expect(testee.vertices).toEqual(expectedVertices));
      it('should have the correct triangles', () => expect(testee.triangles).toEqual(expectedTriangles));
    });

    describe('testText', () => {
      it('should give the right output parameters', () => {
        const testee = new UnitTestSCAD.ThreeDModule({
          openSCADDirectory,
          testText: 'cube([5, 4, 6]);'
        });

        expect(testee.width).toBe(5);
        expect(testee.depth).toBe(4);
        expect(testee.height).toBe(6);
        expect(testee.vertices).toEqual(expectedVertices);
        expect(testee.triangles).toEqual(expectedTriangles);
      });

      it('should give the right output parameters with a centered cube', () => {
        const testee = new UnitTestSCAD.ThreeDModule({
          openSCADDirectory,
          testText: 'cube([2, 2, 2], center=true);'
        });

        expect(testee.width).toBe(2);
        expect(testee.depth).toBe(2);
        expect(testee.height).toBe(2);
        expect(testee.vertices).toEqual([
          [-1, 1, 1],
          [1, -1, 1],
          [1, 1, 1],
          [-1, -1, 1],
          [-1, -1, -1],
          [1, 1, -1],
          [1, -1, -1],
          [-1, 1, -1],
        ]);
        expect(testee.triangles).toEqual([
          [[-1, 1, 1], [1, -1, 1], [1, 1, 1]],
          [[1, -1, 1], [-1, 1, 1], [-1, -1, 1]],
          [[-1, -1, -1], [1, 1, -1], [1, -1, -1]],
          [[1, 1, -1], [-1, -1, -1], [-1, 1, -1]],
          [[-1, -1, -1], [1, -1, 1], [-1, -1, 1]],
          [[1, -1, 1], [-1, -1, -1], [1, -1, -1]],
          [[1, -1, 1], [1, 1, -1], [1, 1, 1]],
          [[1, 1, -1], [1, -1, 1], [1, -1, -1]],
          [[1, 1, -1], [-1, 1, 1], [1, 1, 1]],
          [[-1, 1, 1], [1, 1, -1], [-1, 1, -1]],
          [[-1, -1, -1], [-1, 1, 1], [-1, 1, -1]],
          [[-1, 1, 1], [-1, -1, -1], [-1, -1, 1]]
        ]);
      });
    });

    it('should use variables', () => {
      const testee = new UnitTestSCAD.ThreeDModule({
        openSCADDirectory,
        include: ['cube.scad'],
        variables: { width: 6, depth: 5, height: 7 },
      });

      expect(testee.width).toBe(6);
      expect(testee.depth).toBe(5);
      expect(testee.height).toBe(7);
    });
  });
});
