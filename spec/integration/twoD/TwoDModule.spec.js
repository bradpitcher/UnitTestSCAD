const path = require('path');

const UnitTestSCAD = require('../../../src');

const openSCADDirectory = path.join(__dirname, '_resources');

describe('TwoDModule', () => {
  describe('error', () => {
    it('should error on a bad .scad file', () => {
      expect(() => new UnitTestSCAD.TwoDModule({
        openSCADDirectory: path.join(__dirname, '..', '_resources'),
        use: ['garbage.scad'],
      })).toThrow();
    });
  });

  describe('success', () => {
    const expectedVertices = [
      [0, -0],
      [5, -0],
      [5, -4],
      [0, -4],
    ];

    describe('include', () => {
      let testee;

      beforeEach(() => {
        testee = new UnitTestSCAD.TwoDModule({
          openSCADDirectory,
          include: ['square.scad'],
        });
      });

      it('should have the correct height', () => expect(testee.height).toBe(4));
      it('should have the correct width', () => expect(testee.width).toBe(5));
      it('should have the correct vertices', () => expect(testee.vertices).toEqual(expectedVertices));
    });

    describe('use', () => {
      let testee;

      beforeEach(() => {
        testee = new UnitTestSCAD.TwoDModule({
          openSCADDirectory,
          use: ['square-module.scad'],
          testText: 'mySquare([5, 4]);'
        });
      });

      it('should have the correct height', () => expect(testee.height).toBe(4));
      it('should have the correct width', () => expect(testee.width).toBe(5));
      it('should have the correct vertices', () => expect(testee.vertices).toEqual(expectedVertices));
    });

    describe('setUpText', () => {
      let testee;

      beforeEach(() => {
        testee = new UnitTestSCAD.TwoDModule({
          openSCADDirectory,
          setUpText: 'square([5, 4]);'
        });
      });

      it('should have the correct height', () => expect(testee.height).toBe(4));
      it('should have the correct width', () => expect(testee.width).toBe(5));
      it('should have the correct vertices', () => expect(testee.vertices).toEqual(expectedVertices));
    });

    describe('testText', () => {
      it('should give the right hight/width/vertices', () => {
        const testee = new UnitTestSCAD.TwoDModule({
          openSCADDirectory,
          testText: 'square([5, 4]);'
        });

        expect(testee.height).toBe(4);
        expect(testee.width).toBe(5);
        expect(testee.vertices).toEqual(expectedVertices);
      });

      it('should give the right width/height/vertices for centered models', () => {
        const testee = new UnitTestSCAD.TwoDModule({
          openSCADDirectory,
          testText: 'square([3, 3], center=true);'
        });

        expect(testee.height).toBe(3);
        expect(testee.width).toBe(3);
        expect(testee.vertices).toEqual([
          [-1.5, 1.5],
          [1.5, 1.5],
          [1.5, -1.5],
          [-1.5, -1.5],
        ]);
      });
    });

    it('should use variables', () => {
      const getTestee = (variables) => new UnitTestSCAD.TwoDModule({
        openSCADDirectory,
        include: ['circle.scad'],
        variables,
      });

      expect(getTestee({}).vertices.length).toBe(20);
      expect(getTestee({ $fn: 4 }).vertices.length).toBe(4);
      expect(getTestee({ $fn: 100 }).vertices.length).toBe(100);
    });
  });
});
