const header = 'header';
const mockHeader = {
  getHeader: jest.fn(() => header)
};

const output = 'output';
const mockFileType = {
  execute: jest.fn(() => output)
};

jest.mock('@/file/Header', () => mockHeader);

const AbstractParent = require('@/api/AbstractParent');

describe('AbstractParent', () => {
  describe('sanitise', () => {

    it('should call with values given', () => {
      const openSCADDirectory = 'directory';
      const use = ['a', 'b', 'c'];
      const include = ['x', 'y', 'z'];
      const setUpText = 'setUpText';
      const testText = 'testText';
      const variables = { a: '1', $fn: '100' };

      new AbstractParent({
        openSCADDirectory,
        use,
        include,
        setUpText,
        testText,
        variables,
      }, mockFileType);

      expect(mockHeader.getHeader).toHaveBeenCalledWith(openSCADDirectory, use, include);
      expect(mockFileType.execute).toHaveBeenCalledWith(header, setUpText, testText, variables);
    });

    it('should use default values when not defined', () => {
      new AbstractParent({}, mockFileType);

      expect(mockHeader.getHeader).toHaveBeenCalledWith('', [], []);
      expect(mockFileType.execute).toHaveBeenCalledWith(header, '', '', {});
    });
  });

  describe('output', () => {
    it('should get the output from file execution', () => {
      const testee = new AbstractParent({}, mockFileType);

      expect(testee.output).toBe(output);
    });
  });
});
