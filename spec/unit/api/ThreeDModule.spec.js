const path = require('path');
const fs = require('fs');

const output = fs.readFileSync(path.join(__dirname, '..', '_resources', 'threed-output.stl'), 'utf-8');
const mockThreeDModuleFile = {
  execute: jest.fn(() => output)
};

jest.mock('@/file/ThreeDModuleFile', () => mockThreeDModuleFile);

const ThreeDModule = require('@/api/ThreeDModule');

describe('ThreeDModule', () => {
  let testee;
  beforeEach(() => {
    testee = new ThreeDModule();
  });

  it('should get the vertices', () => expect(testee.vertices).toEqual([
    [0, 7, 2],
    [3, 0, 2],
    [3, 7, 2],
    [0, 0, 2],
    [0, 0, 0],
    [3, 7, 0],
    [3, 0, 0],
    [0, 7, 0],
  ]));
  it('should have the correct width', () => expect(testee.width).toBe(3));
  it('should have the correct height', () => expect(testee.depth).toBe(7));
  it('should have the correct depth', () => expect(testee.height).toBe(2));
  it('should have the correct triangles', () => expect(testee.triangles).toEqual([
    [[0, 7, 2], [3, 0, 2], [3, 7, 2]],
    [[3, 0, 2], [0, 7, 2], [0, 0, 2]],
    [[0, 0, 0], [3, 7, 0], [3, 0, 0]],
    [[3, 7, 0], [0, 0, 0], [0, 7, 0]],
    [[0, 0, 0], [3, 0, 2], [0, 0, 2]],
    [[3, 0, 2], [0, 0, 0], [3, 0, 0]],
    [[3, 0, 2], [3, 7, 0], [3, 7, 2]],
    [[3, 7, 0], [3, 0, 2], [3, 0, 0]],
    [[3, 7, 0], [0, 7, 2], [3, 7, 2]],
    [[0, 7, 2], [3, 7, 0], [0, 7, 0]],
    [[0, 0, 0], [0, 7, 2], [0, 7, 0]],
    [[0, 7, 2], [0, 0, 0], [0, 0, 2]],
  ]));
});
