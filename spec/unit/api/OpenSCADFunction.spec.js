const Types = require('@/types/Types');

const output = 'output';
const types = [
  {
    type: Types.BOOLEAN,
    method: 'isBoolean'
  }, {
    type: Types.INF,
    method: 'isInf'
  }, {
    type: Types.NAN,
    method: 'isNan'
  }, {
    type: Types.NUMBER,
    method: 'isNumber'
  }, {
    type: Types.RANGE,
    method: 'isRange'
  }, {
    type: Types.STRING,
    method: 'isString'
  }, {
    type: Types.UNDEF,
    method: 'isUndef'
  }, {
    type: Types.VECTOR,
    method: 'isVector'
  }
];

const mockFunctionFile = {
  execute: jest.fn(() => output)
};

const mockTypeConverter = {
  getType: jest.fn()
};

jest.mock('@/file/FunctionFile', () => mockFunctionFile);
jest.mock('@/types/TypeConverter', () => mockTypeConverter);

const OpenSCADFunction = require('@/api/OpenSCADFunction');

describe('OpenSCADFunction', () => {
  let testee;

  types.forEach(type => {
    describe(`${type.type}`, () => {
      beforeEach(() => {
        mockTypeConverter.getType.mockReturnValue(type.type);
        testee = new OpenSCADFunction();
      });

      it('should get the type from TypeConverter', () => {
        expect(testee.type).toBe(type.type);
        expect(mockTypeConverter.getType).toHaveBeenCalledWith(output);
      });


      it(`${type.method} should be true`, () =>

        expect(testee[type.method]()).toBe(true, type.method));

      types
        .filter(notType => notType !== type)
        .forEach(notType => {
          it(`${notType.method} should be false`, () =>

            expect(testee[notType.method]()).toBe(false, notType.method));
        });
    });
  });
});
