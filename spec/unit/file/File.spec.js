const {EOL} = require('os');

const options = {
  header: 'header',
  setUpText: 'setUpText',
  testText: 'testText',
  variables: { a: 1, text: '"sometext"', $fn: 100 },
};

const format = 'asciistl';
const stlOutput = 'stloutput';
const openscadOutput = 'output';
const error = 'error';

let mockChildProcess, File;

describe('File', () => {
  jest.mock('child_process', () => mockChildProcess);

  beforeEach(() => {
    mockChildProcess = {
      spawnSync: jest.fn(),
    };

    File = require('@/file/File');
  });

  afterEach(jest.resetModules);

  describe('openscad executes successfully', () => {
    beforeEach(() => {
      mockChildProcess.spawnSync.mockReturnValue({
        stderr: { toString: jest.fn(() => openscadOutput) },
        stdout: { toString: jest.fn(() => stlOutput) },
      });
    });

    it('should write the file, execute, and return the output', () => {
      const expectedOptions = {
        input: `${options.header}${EOL}${options.setUpText}${EOL}${options.testText}`,
        shell: true,
      };
      const result = File.execute(options, format);

      const spyArgs = mockChildProcess.spawnSync.mock.calls[0];
      expect(result).toEqual({ out: openscadOutput, file: stlOutput });
      expect(spyArgs.length).toBe(3);
      expect(spyArgs[0]).toBe('openscad');
      expect(spyArgs[1].length).toBe(11);
      expect(spyArgs[1].join(' ').indexOf(`-o - --export-format ${format}`)).toBe(0);
      expect(spyArgs[1].join(' ')).toContain('-D a="1"');
      expect(spyArgs[1].join(' ')).toContain('-D text="\\"sometext\\""');
      expect(spyArgs[1].join(' ')).toContain('-D \\$fn="100"');
      expect(spyArgs[1].pop()).toBe('-');
      expect(spyArgs[2]).toEqual(expectedOptions);
    });
  });

  describe('openscad fails to execute', () => {
    it('should re-throw error', () => {
      mockChildProcess.spawnSync.mockImplementation(() => {
        throw new Error(error);
      });

      expect(() => File.execute(options, format)).toThrow(error);
    });
  });
});
