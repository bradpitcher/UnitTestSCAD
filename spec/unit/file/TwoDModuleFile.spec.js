const format = 'svg';
const header = 'header';
const setUpText = 'setUpText';
const testText = 'testText';
const variables = { b: '[0, 1]', c: 'test' };

const file = 'file';
const output = {file};
const mockFile = {
  execute: jest.fn(() => output)
};

jest.mock('@/file/File', () => mockFile);

const TwoDModuleFile = require('@/file/TwoDModuleFile');

describe('TwoDModuleFile', () => {
  it('should call through to File.execute', () => {
    const result = TwoDModuleFile.execute(header, setUpText, testText, variables);

    expect(result).toBe(file);
    expect(mockFile.execute).toHaveBeenCalledWith({
      header,
      setUpText,
      testText,
      variables,
    }, format);
  });
});
