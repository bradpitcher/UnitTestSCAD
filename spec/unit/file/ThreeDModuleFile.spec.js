const format = 'asciistl';
const header = 'header';
const setUpText = 'setUpText';
const testText = 'testText';
const variables = { one: '1', two: '2.0' };

const file = 'file';
const output = {file};
const mockFile = {
  execute: jest.fn(() => output)
};

jest.mock('@/file/File', () => mockFile);

const ThreeDModuleFile = require('@/file/ThreeDModuleFile');

describe('ThreeDModuleFile', () => {
  it('should call through to File.execute', () => {
    const result = ThreeDModuleFile.execute(header, setUpText, testText, variables);

    expect(result).toBe(file);
    expect(mockFile.execute).toHaveBeenCalledWith({
      header,
      setUpText,
      testText,
      variables,
    }, format);
  });
});
