const {EOL} = require('os');

const format = 'asciistl';
const startMarker = '"UnitTestSCAD __start_marker__"';
const endMarker = '"UnitTestSCAD __end_marker__"';
const failurePrevention = 'cube(1);';

const header = 'header';
const setUpText = 'setUpText';
const testText = 'testText';

const wrappedTestText = [
  `echo(${startMarker});`,
  `echo(${testText});`,
  `echo(${endMarker});`,
  failurePrevention
].join(EOL);

const mockFile = {
  execute: jest.fn(() => ({
    out: `ECHO: ${startMarker}${EOL}ECHO: Poop${EOL}ECHO: Boop${EOL}ECHO: Scoop${EOL}ECHO: ${endMarker}`
  }))
};

jest.mock('@/file/File', () => mockFile);

const FunctionFile = require('@/file/FunctionFile');

describe('FunctionFile', () => {
  it('should wrap and unwrap the text', () => {
    const output = FunctionFile.execute(header, setUpText, testText);

    // Idk why but `.toHaveBeenCalledWith()` really didn't like `wrappedTestText`'s content
    expect(mockFile.execute.mock.calls[mockFile.execute.mock.calls.length - 1][0].header).toBe(header);
    expect(mockFile.execute.mock.calls[mockFile.execute.mock.calls.length - 1][0].setUpText).toBe(setUpText);
    expect(mockFile.execute.mock.calls[mockFile.execute.mock.calls.length - 1][0].testText).toBe(wrappedTestText);
    expect(mockFile.execute.mock.calls[mockFile.execute.mock.calls.length - 1][1]).toBe(format);

    expect(output).toBe(`Poop${EOL}Boop${EOL}Scoop`);
  });

  it('should sanitise wrap and unwrap the text', () => {
    const output = FunctionFile.execute(header, setUpText, testText + ';');

    expect(mockFile.execute.mock.calls[mockFile.execute.mock.calls.length - 1][0].header).toBe(header);
    expect(mockFile.execute.mock.calls[mockFile.execute.mock.calls.length - 1][0].setUpText).toBe(setUpText);
    expect(mockFile.execute.mock.calls[mockFile.execute.mock.calls.length - 1][0].testText).toBe(wrappedTestText);
    expect(mockFile.execute.mock.calls[mockFile.execute.mock.calls.length - 1][1]).toBe(format);

    expect(output).toBe(`Poop${EOL}Boop${EOL}Scoop`);
  });
});
