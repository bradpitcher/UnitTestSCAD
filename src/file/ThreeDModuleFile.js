const File = require('./File');

module.exports = {
  execute(header, setUpText, testText, variables) {
    return File.execute({
      header,
      setUpText,
      testText,
      variables,
    }, 'asciistl').file;
  }
};
