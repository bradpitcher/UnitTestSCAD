const {EOL} = require('os');
const {spawnSync} = require('child_process');

function executeOpenSCAD(scad, format, vars) {
  const args = ['-o', '-', '--export-format', format].concat(
    ...Object.keys(vars).map(key => {
      let value = vars[key];
      if (typeof value === 'string') {
        // Escape string quotes for use on the command line
        value = value.replace(/^"(.*)"$/, '\\"$1\\"');
      }
      return ['-D', `${key.replace('$', '\\$')}="${value}"`];
    }),
    '-'
  );
  const options = { input: scad, shell: true };
  const { stderr, stdout } = spawnSync('openscad', args, options);
  return {
    out: stderr.toString(),
    file: stdout.toString(),
  };
}

module.exports = {
  execute(options, format) {
    const { header, setUpText, testText, variables } = options;
    return executeOpenSCAD(
      `${header}${EOL}${setUpText}${EOL}${testText}`.trim(),
      format,
      variables
    );
  }
};
